package com.saviynt.custom.sqlbuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.saviynt.custom.sqlbuilder.model.Column;
import com.saviynt.custom.sqlbuilder.model.JoinCondition;
import com.saviynt.custom.sqlbuilder.model.Relation;
import com.saviynt.custom.sqlbuilder.model.RelationColumn;
import com.saviynt.custom.sqlbuilder.model.Table;

/**
 * The Class SqlBuilder.
 */
public class SqlBuilder {

	/** The connection. */
	private static Connection connection;
	
	/** The table list. */
	private static List<Table> tableList;
	
	/** The alias names. */
	private static List<String> aliasNames;
	
	private static List<Column> columnList;
	
	private static List<Relation> relationList;
	
	private static Integer columnCount = 0;
	
	private static Integer relationCount = 1;
	
	private static Map<String, Integer> tableNameIdMap = new HashMap<String, Integer>(); 
	
	private static List<String> relationcacheList;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		System.out.println("### Start Building schema ###");
		System.out.println("Open Connection!!!");
		Map<String, String> resultMap = getTableJson();
		/*
		 * System.out.println("Table JSON : ");
		 * System.out.println(resultMap.get("table"));
		 * System.out.println("Column JSON : ");
		 * System.out.println(resultMap.get("column"));
		 */
		
		FileUtility utility = new FileUtility();
		utility.saveJsonFile(resultMap.get("table"), "table");
		utility.saveJsonFile(resultMap.get("column"), "column");
		utility.saveJsonFile(resultMap.get("relation"), "relation");
		
		System.out.println("!!!! Process Completed !!!!");
		
	}
	
	

	/**
	 * Gets the connection.
	 *
	 * @return the connection
	 */
	public static Connection getConnection() {
		try {
			if (connection == null) {
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/blankdb2", "root", "password");
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return connection;
	}
	
	/**
	 * Close connection.
	 */
	public static void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Get SSM table metadata json for SQL Builder.
	 *
	 * @return tableJson
	 */
	public static Map<String, String> getTableJson() {
		String tableJson = "";
		String columnJson = "";
		String relationJson = "";
		try {
			Connection conn = getConnection();

			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery("show tables");
			
			int counter = 1;
			tableList = new ArrayList<Table>();
			while (resultSet.next()) {
				Table table = new Table();
				table.setId(counter);
				table.setName(resultSet.getString(1));
				table.setAlias(getAliasName(resultSet.getString(1)));
				tableNameIdMap.put(StringUtils.lowerCase(resultSet.getString(1)), counter);
				tableList.add(table);
				getColumns(resultSet.getString(1), counter);
				counter++;
			}
			for (Table table : tableList) {
				getTableConstraints(table.getName());
			}
			
			closeConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(tableList != null && tableList.size() > 0) {
			GsonBuilder builder = new GsonBuilder();
			builder.setPrettyPrinting();
			builder.disableHtmlEscaping();
			Gson gson = builder.create();
			tableJson = gson.toJson(tableList);
			columnJson = gson.toJson(columnList);
			relationJson = gson.toJson(relationList);
		}
		Map<String, String> resultMap = new HashMap<String, String>();
		resultMap.put("table", tableJson);
		resultMap.put("column", columnJson);
		resultMap.put("relation", relationJson);
		return resultMap;
	}
	
	private static void getTableConstraints(String tablename) {
		try {
			String schema = "blankdb2";
			if (relationcacheList == null) {
				relationcacheList = new ArrayList<String>();
			}
			if (relationList == null) {
				relationList = new ArrayList<Relation>();
			}
			if (isCompositeKeyTable(tablename, schema)) {
				checkAndUpdateRelationforCompositeKey(tablename, schema);
				checkAndUpdateRelationForForeignKey(tablename, schema);
			} else {
				checkAndUpdateRelationForForeignKey(tablename, schema);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}



	private static void checkAndUpdateRelationForForeignKey(String tablename, String schema) {
		try {

			ResultSet constraintSet = getColumnNames(tablename, schema, false);
			while (constraintSet.next()) {
				String columnName = constraintSet.getString(1);
				String referencedColumnName = constraintSet.getString(3);
				String referencedTableName = constraintSet.getString(4);
				Integer tableId = tableNameIdMap.get(StringUtils.lowerCase(tablename));
				Integer refTableId = tableNameIdMap.get(StringUtils.lowerCase(referencedTableName));
				if(!(relationcacheList.contains(tableId + "_"+ refTableId) || relationcacheList.contains(refTableId + "_"+ tableId))) {
					relationcacheList.add(tableId + "_"+ refTableId);
					Relation relation = new Relation();
					relation.setId(relationCount++);
					relation.setLeftTable(tableId);
					relation.setRightTable(refTableId);
					if(tableId == refTableId) {
						relation.setType("SELF");
					} else {
						relation.setType("INNER");
					}
					List<JoinCondition> joinConditions = new ArrayList<JoinCondition>();
					JoinCondition joinCondition = new JoinCondition();
					joinCondition.setOperator("=");
					joinCondition.setLeftExpr(new RelationColumn(columnName));
					joinCondition.setRightExpr(new RelationColumn(referencedColumnName));
					joinConditions.add(joinCondition);
					relation.setJoinConditions(joinConditions);
					relationList.add(relation);
				} else {
					for (Relation relation : relationList) {
						if ((relation.getLeftTable() == tableId && relation.getRightTable() == refTableId)
								|| relation.getLeftTable() == refTableId && relation.getRightTable() == tableId) {
							List<JoinCondition> joinConditions = relation.getJoinConditions();
							JoinCondition condn2 = new JoinCondition();
							condn2.setLeftExpr(new RelationColumn(columnName));
							condn2.setRightExpr(new RelationColumn(referencedColumnName));
							condn2.setOperator("=");
							joinConditions.add(condn2);
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}



	/**
	 * Gets the alias name.
	 *
	 * @param tableName the table name
	 * @return the alias name
	 */
	private static String getAliasName(String tableName) {
		String alias = "";
		if(tableName != null && tableName != "") {
			if(tableName.contains("_")) {
				String[] tableNameArr = tableName.split("_");
				for (int i = 0; i < tableNameArr.length; i++) {
					alias = alias + tableNameArr[i].charAt(0);
				}
				
			} else {
				alias = tableName.substring(0,3);
			}
		}
		return checkDuplicate(alias);
	}
	
	/**
	 * Check duplicate.
	 *
	 * @param alias the alias
	 * @return the string
	 */
	private static String checkDuplicate(String alias) {
		String result = "";
		if(aliasNames == null) {
			aliasNames = new ArrayList<String>();
		}
		if(aliasNames.contains(alias)) {
			boolean breakIt = false;
			int counter = 1;
			while (!breakIt) {
				result = alias + counter;
				counter++;
				if(!aliasNames.contains(result)) {
					breakIt = true;
				}
			}
		} else {
			aliasNames.add(alias);
			return alias;
		}
		return result;
	}
	
	public static List<Column> getColumns(String tablename, Integer tableId) {
		String schemaName = "saviynt";
		String columnquery = "SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='"
				+  schemaName + "' AND `TABLE_NAME`='" + tablename + "'";
		Connection conn = getConnection();
		Statement stmt;
		try {
			stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(columnquery);
			if(columnList == null) {
				columnList = new ArrayList<Column>();
			}
			while (resultSet.next()) {
				String columnName = resultSet.getString(1);
				Column column = new Column();
				column.setId(columnCount++);
				column.setName(columnName);
				column.setDisplay(columnName.replace("_", " "));
				column.setTable(tableId);
				column.setAlias(columnName.replace("_", ""));
				columnList.add(column);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			//closeConnection();
		}

		return null;
	}
	
	public static void checkAndUpdateRelationforCompositeKey(String tablename,String schema) {
		try {
			
			ResultSet constraintSet = getColumnNames(tablename, schema, true);
			while(constraintSet.next()) {
				String columnName = constraintSet.getString(1);
				Integer tableId = tableNameIdMap.get(StringUtils.lowerCase(tablename));
				
				Connection conn = getConnection();
				Statement statement = conn.createStatement();
				String tablenamequery = "select TABLE_NAME from information_schema.`COLUMNS` c where c.COLUMN_NAME = '"+columnName+"' and COLUMN_KEY = 'PRI' and TABLE_SCHEMA = '"+schema+"' and EXTRA = 'auto_increment' ";
				ResultSet tableNameList = statement.executeQuery(tablenamequery);
				String ref_tablename = null;
				while (tableNameList.next()) {
					if(tableNameList.getString(1) != null) {
						ref_tablename = tableNameList.getString(1);
						break;
					}
				}
				Integer refTableId = tableNameIdMap.get(ref_tablename);
				Relation relationUse = null;
				
				if(!(relationcacheList.contains(tableId + "_" + refTableId) || relationcacheList.contains(tableId + "_" + refTableId))) {
					relationcacheList.add(tableId + "_" + refTableId);
					relationUse = new Relation();
					relationUse.setId(relationCount++);
					relationUse.setLeftTable(tableId);
					relationUse.setType("INNER");
					relationUse.setRightTable(tableNameIdMap.get(ref_tablename));
					List<JoinCondition> joinConditionsList = relationUse.getJoinConditions();
					JoinCondition condition = new JoinCondition();
					condition.setLeftExpr(new RelationColumn(columnName));
					condition.setRightExpr(new RelationColumn(columnName));
					condition.setOperator("=");
					joinConditionsList.add(condition);
					relationUse.setJoinConditions(joinConditionsList);
					relationList.add(relationUse);
				} else {
					/*//Currently No Required####
					 * 
					 * for (Relation relation : relationList) { if((relation.getLeftTable() ==
					 * tableId && relation.getRightTable() == refTableId) || relation.getLeftTable()
					 * == refTableId && relation.getRightTable() == tableId) { List<JoinCondition>
					 * joinConditions = relation.getJoinConditions(); } }
					 */
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static boolean isCompositeKeyTable(String tablename, String schemaName) {
		boolean isComposite = false;
		Connection connection = getConnection();
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT COUNT(*) num_keys  FROM   information_schema.KEY_COLUMN_USAGE WHERE table_name ='"
					+ tablename + "' AND constraint_name = 'PRIMARY' and TABLE_SCHEMA = '" + schemaName + "'";
			ResultSet resultSet = statement.executeQuery(query);
			while(resultSet.next()) {
				Integer count = resultSet.getInt(1);
				if (count > 1) {
					isComposite = true;
					break;
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
		}
		return isComposite;
	}
	
	
	public static ResultSet getColumnNames(String tablename, String schema, boolean isPrimary) {
		ResultSet constraintSet = null;
		String constraintQuery = "select COLUMN_NAME, CONSTRAINT_NAME, REFERENCED_COLUMN_NAME, REFERENCED_TABLE_NAME from information_schema.KEY_COLUMN_USAGE where TABLE_NAME = '"+tablename+"' and `TABLE_SCHEMA`='"+schema+"' ";
		if(isPrimary) {
			constraintQuery = constraintQuery + " and CONSTRAINT_NAME = 'PRIMARY'";
		} else {
			constraintQuery = constraintQuery + " and CONSTRAINT_NAME like 'FK%'";
		}
		Connection conn = getConnection();
		try {
			Statement stmt = conn.createStatement();
			constraintSet = stmt.executeQuery(constraintQuery);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return constraintSet;
	}

}
