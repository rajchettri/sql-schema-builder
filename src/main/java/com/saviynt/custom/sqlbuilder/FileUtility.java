package com.saviynt.custom.sqlbuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileUtility {
	
	private static Map<String, String> filePathMap = new HashMap<String, String>();
	
	{
		filePathMap.put("table", "C:\\Saviynt\\Conf\\table.json");
		filePathMap.put("column", "C:\\Saviynt\\Conf\\column.json");
		filePathMap.put("relation", "C:\\Saviynt\\Conf\\relation.json");
	}
	
	public void saveJsonFile(String jsonString, String key) {
        try {
        	FileWriter file = new FileWriter(filePathMap.get(key));
        	
			file.write(jsonString);
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}

}
