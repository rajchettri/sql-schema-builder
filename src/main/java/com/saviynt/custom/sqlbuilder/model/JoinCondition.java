package com.saviynt.custom.sqlbuilder.model;

public class JoinCondition {
	
    private RelationColumn leftExpr;
	private RelationColumn rightExpr;
	private String operator;
	
	public RelationColumn getLeftExpr() {
		return leftExpr;
	}
	public void setLeftExpr(RelationColumn leftExpr) {
		this.leftExpr = leftExpr;
	}
	public RelationColumn getRightExpr() {
		return rightExpr;
	}
	public void setRightExpr(RelationColumn rightExpr) {
		this.rightExpr = rightExpr;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
}
