package com.saviynt.custom.sqlbuilder.model;

import java.util.ArrayList;
import java.util.List;

public class Relation {
	
	private Integer id;
	private Integer leftTable;
	private Integer rightTable;
	private String type;
    private List<JoinCondition> joinConditions;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getLeftTable() {
		return leftTable;
	}
	public void setLeftTable(Integer leftTable) {
		this.leftTable = leftTable;
	}
	public Integer getRightTable() {
		return rightTable;
	}
	public void setRightTable(Integer rightTable) {
		this.rightTable = rightTable;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<JoinCondition> getJoinConditions() {
		if(joinConditions == null) {
			joinConditions = new ArrayList<JoinCondition>();
		}
		return joinConditions;
	}
	public void setJoinConditions(List<JoinCondition> joinConditions) {
		this.joinConditions = joinConditions;
	}
    
    
    

}
