package com.saviynt.custom.sqlbuilder.model;

public class RelationColumn {
	
	private String colName;
	
	public RelationColumn() {
		
	}
	
	public RelationColumn(String colName) {
		this.colName = colName;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

}
